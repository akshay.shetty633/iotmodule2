**IIOT Protocols**

     The Fundamentals of 4-20 mA Current Loops

As major as the 4-20 mA loop standard has become in the process control industry, many do not understand the fundamentals of its setup and use. Not knowing the basics could potentially cost you money when it comes time to make decisions about process display and control. Having a grasp on the history, workings, pros and cons of the 4-20 mA loop will help you to understand why it is the dominant standard for the industry and allow you to make informed decisions about your process control.


How Does a 4-20 mA Current Loop Work?

In order to understand what a 4-20 mA direct current (DC) loop is and how it works, we will need to know a little bit of math. Don't worry; we won't be delving into any advanced electrical engineering formulas. In fact, the formula we need is relatively simple: V = I x R. This is Ohm's Law. What this is saying is that the voltage (V) is equal to the current (I) multiplied by the resistance (R) ("I" stands for Intensité de Courant, French for Current Intensity). This is the fundamental equation in electrical engineering.

  ![ALT](https://ni.scene7.com/is/image/ni/Latest_-_01?scl=1)


Advantages


*The 4-20 mA current loop is the dominant standard in many industries.
*It is the simplest option to connect and configure.
*It uses less wiring and connections than other signals, greatly reducing initial setup costs.
*Better for traveling long distances, as current does not degrade over long connections like voltage.
*It is less sensitive to background electrical noise.
*Since 4 mA is equal to 0% output, it is incredibly simple to detect a fault in the system.

Disadvantage

*Current loops can only transmit one particular process signal.
*Multiple loops must be created in situations where there are numerous process variables that require transmission. Running so much wire could lead to *problems with ground loops if independent loops are not properly isolated.
*These isolation requirements become exponentially more complicated as the number of loops increases.

**Modbus Communication Protocol**


Modbus is a communication protocol for use with programmable logic controllers (PLC). It is typically used to transmit signals from instrumentation and control devices back to a main controller, or data gathering system.


The method is used for transmitting information between electronic devices. The device requesting information is called “master” and “slaves” are the devices supplying information.


In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.

How does Modbus protocol works

Communication between a master and a slave occurs in a frame that indicates a function code.


The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.


The slave then responds, based on the function code received.


The protocol is commonly used in IoT as a local interface to manage devices.


Modbus protocol can be used over 2 interfaces

*RS485 - called as Modbus RTU
*Ethernet - called as Modbus TCP/IP


**MQTT (Message Queuing Telemetry Transport)**

*A simple messaging protocol designed for constrained devices with low bandwidth.
*A lightweight publish and subscribe system to publish and receive messages as a client.
*A perfect solution for IOT applications, also allows to send commands to control outputs, read and publish data from sensor nodes.
*Communication between several devices can be established simultaneously.

How MQTT Works:

!![ALT](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/358848b4b9222917e31db143ff0a6383/Main-entities-of-the-Message-Queuing-Telemetry-Transport-MQTT-protocol.png)

MQTT Client as a publisher sends a message to the MQTT broker whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.

**HTTP ( Hyper Text Transport Protocol)**


*It is a request response protocol
*The client sends an HTTP request
*The server sends back a HTTP response

![ALT](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/85f443d40e36e1f5bcb4e71ac14205b0/http-req-res.png)


The Request
request has 3 parts

*Request line
*HTTP headrs
*message body
*GET request

it is a type of HTTP request using the GET method

there are different types of methods
GET
     Retrieve the resource from the server (e.g. when visiting a page);
POST
     Create a resource on the server (e.g. when submitting a form);
PUT/PATCH
     Update the resource on the server (used by APIs);
DELETE
     Delete the resource from the server (used by APIs).

**The response**

response is made of 3 parts

Status line
HTTP header
Message body

The server will send you send you different status codes depending on situation

![ALT](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/1dcbcc2c7e58e59a042e55b2b2080405/restful-web-services-with-spring-mvc-28-638.jpg)
